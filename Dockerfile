FROM node:16 as build-stage

WORKDIR /app

COPY . /app

RUN yarn install
COPY . .
RUN yarn build

FROM nginx:1.19
COPY --from=build-stage /app/build/ /usr/share/nginx/html
COPY --from=build-stage /app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080